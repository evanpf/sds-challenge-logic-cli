package com.Frabell;


import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    private Main test;

    @Test
    public void getQuality() {
        assertEquals("Input: getQuality(1, 0)", "The +5 Dexterity Vest will have a quality rating of 20 in 0 days.", test.getQuality(1, 0));
        assertEquals("Input: getQuality(1, 2)", "The +5 Dexterity Vest will have a quality rating of 18 in 2 days.", test.getQuality(1, 2));
        assertEquals("Input: getQuality(1, 12)", "The +5 Dexterity Vest will have a quality rating of 6 in 12 days.", test.getQuality(1, 12));
        assertEquals("Input: getQuality(1, 14)", "The +5 Dexterity Vest will have a quality rating of 2 in 14 days.", test.getQuality(1, 14));
        assertEquals("Input: getQuality(1, 15)", "The +5 Dexterity Vest will have a quality rating of 0 in 15 days.", test.getQuality(1, 15));
        assertEquals("Input: getQuality(1, 20)", "The +5 Dexterity Vest will have a quality rating of 0 in 20 days.", test.getQuality(1, 20));
        assertEquals("Input: getQuality(1, 100)", "The +5 Dexterity Vest will have a quality rating of 0 in 100 days.", test.getQuality(1, 100));

        assertEquals("Input: getQuality(2, 0)", "The Aged Brie will have a quality rating of 0 in 0 days.", test.getQuality(2, 0));
        assertEquals("Input: getQuality(2, 1)", "The Aged Brie will have a quality rating of 1 in 1 days.", test.getQuality(2, 1));
        assertEquals("Input: getQuality(2, 2)", "The Aged Brie will have a quality rating of 2 in 2 days.", test.getQuality(2, 2));
        assertEquals("Input: getQuality(2, 3)", "The Aged Brie will have a quality rating of 0 in 3 days.", test.getQuality(2, 3));
        assertEquals("Input: getQuality(2, 4)", "The Aged Brie will have a quality rating of 0 in 4 days.", test.getQuality(2, 4));
        assertEquals("Input: getQuality(2, 12)", "The Aged Brie will have a quality rating of 0 in 12 days.", test.getQuality(2, 12));
        assertEquals("Input: getQuality(2, 14)", "The Aged Brie will have a quality rating of 0 in 14 days.", test.getQuality(2, 14));
        assertEquals("Input: getQuality(2, 15)", "The Aged Brie will have a quality rating of 0 in 15 days.", test.getQuality(2, 15));
        assertEquals("Input: getQuality(2, 20)", "The Aged Brie will have a quality rating of 0 in 20 days.", test.getQuality(2, 20));
        assertEquals("Input: getQuality(2, 100)", "The Aged Brie will have a quality rating of 0 in 100 days.", test.getQuality(2, 100));

        assertEquals("Input: getQuality(3, 0)", "The Elixir of the Mongoose will have a quality rating of 7 in 0 days.", test.getQuality(3, 0));
        assertEquals("Input: getQuality(3, 2)", "The Elixir of the Mongoose will have a quality rating of 5 in 2 days.", test.getQuality(3, 2));
        assertEquals("Input: getQuality(3, 2)", "The Elixir of the Mongoose will have a quality rating of 0 in 7 days.", test.getQuality(3, 7));
        assertEquals("Input: getQuality(3, 2)", "The Elixir of the Mongoose will have a quality rating of 0 in 8 days.", test.getQuality(3, 8));
        assertEquals("Input: getQuality(3, 12)", "The Elixir of the Mongoose will have a quality rating of 0 in 12 days.", test.getQuality(3, 12));
        assertEquals("Input: getQuality(3, 14)", "The Elixir of the Mongoose will have a quality rating of 0 in 14 days.", test.getQuality(3, 14));
        assertEquals("Input: getQuality(3, 15)", "The Elixir of the Mongoose will have a quality rating of 0 in 15 days.", test.getQuality(3, 15));
        assertEquals("Input: getQuality(3, 20)", "The Elixir of the Mongoose will have a quality rating of 0 in 20 days.", test.getQuality(3, 20));
        assertEquals("Input: getQuality(3, 100)", "The Elixir of the Mongoose will have a quality rating of 0 in 100 days.", test.getQuality(3, 100));

        assertEquals("Input: getQuality(4, 0)", "The Sulfuras will have a quality rating of 80 in 0 days.", test.getQuality(4, 0));
        assertEquals("Input: getQuality(4, 2)", "The Sulfuras will have a quality rating of 80 in 2 days.", test.getQuality(4, 2));
        assertEquals("Input: getQuality(4, 12)", "The Sulfuras will have a quality rating of 80 in 12 days.", test.getQuality(4, 12));
        assertEquals("Input: getQuality(4, 14)", "The Sulfuras will have a quality rating of 80 in 14 days.", test.getQuality(4, 14));
        assertEquals("Input: getQuality(4, 15)", "The Sulfuras will have a quality rating of 80 in 15 days.", test.getQuality(4, 15));
        assertEquals("Input: getQuality(4, 20)", "The Sulfuras will have a quality rating of 80 in 20 days.", test.getQuality(4, 20));
        assertEquals("Input: getQuality(4, 100)", "The Sulfuras will have a quality rating of 80 in 100 days.", test.getQuality(4, 100));

        assertEquals("Input: getQuality(5, 0)", "The Concert backstage passes will have a quality rating of 20 in 0 days.", test.getQuality(5, 0));
        assertEquals("Input: getQuality(5, 2)", "The Concert backstage passes will have a quality rating of 22 in 2 days.", test.getQuality(5, 2));
        assertEquals("Input: getQuality(5, 12)", "The Concert backstage passes will have a quality rating of 42 in 12 days.", test.getQuality(5, 12));
        assertEquals("Input: getQuality(5, 14)", "The Concert backstage passes will have a quality rating of 44 in 14 days.", test.getQuality(5, 14));
        assertEquals("Input: getQuality(5, 15)", "The Concert backstage passes will have a quality rating of 45 in 15 days.", test.getQuality(5, 15));
        assertEquals("Input: getQuality(5, 20)", "The Concert backstage passes will have a quality rating of 0 in 20 days.", test.getQuality(5, 20));
        assertEquals("Input: getQuality(5, 100)", "The Concert backstage passes will have a quality rating of 0 in 100 days.", test.getQuality(5, 100));
    }
}