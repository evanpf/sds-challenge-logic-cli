package com.Frabell;

//The quality of an item is never over 50?  But Sulfuras start at a quality of 80?

import java.util.*;

public class Main {

    public static void main(String[] args) {

        Scanner inputReader = new Scanner(System.in);

        String leftAlignFormat = "%-20s %-10s %-26s %-60s";
        System.out.format(leftAlignFormat, "#", "Item", "Days Until Expiration", "Quality Points");
        System.out.println();
        System.out.println("----------------------------------------------------------------------------");

        HashMap<Integer, List<Object>> values = inventoryStack();

        for (int i = 0; i <= values.size(); ++i) {
            String brace = ")";
            if (values.containsKey(i)) {
                String itemName = values.get(i).subList(0, 1).toString().replace("[", "").replace("]", "");
                String days = values.get(i).subList(1, 2).toString().replace("[", "").replace("]", "");
                int expirationDate = Integer.parseInt(days);
                String initialQ = values.get(i).subList(2, 3).toString().replace("[", "").replace("]", "");
                int initialQuality = Integer.parseInt(initialQ);
                System.out.format("%1d%1s%26s%15d%24d", i, brace, itemName, expirationDate, initialQuality);
                System.out.println();
            }
        }

        System.out.println();
        System.out.print("Please enter an item number (1-5): ");
        String userEntry = inputReader.nextLine();
        int item = Integer.parseInt(userEntry);

        System.out.println();
        System.out.print("How many days in the future would you like to forecast the quality of selected item? (1-100):   ");
        String userInput = inputReader.nextLine();
        int daysFromNow = Integer.parseInt(userInput);

        System.out.println(getQuality(item, daysFromNow));
    }


    public static String getQuality(int item, int daysFromNow) {
        HashMap<Integer, List<Object>> values = inventoryStack();               //I didn't want to repeat code! I swear!

        String itemName = values.get(item).subList(0, 1).toString().replace("[", "").replace("]", "");
        String days = values.get(item).subList(1, 2).toString().replace("[", "").replace("]", "");
        int expirationDate = Integer.parseInt(days);
        String initialQ = values.get(item).subList(2, 3).toString().replace("[", "").replace("]", "");
        int initialQuality = Integer.parseInt(initialQ);

        switch (item) {
            case 1:     //Dexterity Vest
                int quality = qualityDecrease(initialQuality, daysFromNow, expirationDate);
                System.out.println();
                return "The " + itemName + " will have a quality rating of " + quality + " in " + daysFromNow + " days.";

            case 2:     //Does the aged brie degrade after sell date or keep incrementing?
                quality = qualityIncreaseDecrease(initialQuality, daysFromNow, expirationDate);
                System.out.println();
                return "The " + itemName + " will have a quality rating of " + quality + " in " + daysFromNow + " days.";

            case 3:        //mongoose elixir
                quality = qualityDecrease(initialQuality, daysFromNow, expirationDate);
                System.out.println();
                return "The " + itemName + " will have a quality rating of " + quality + " in " + daysFromNow + " days.";

            case 4:     //sulfuras
                quality = initialQuality;
                System.out.println();
                return "The " + itemName + " will have a quality rating of " + quality + " in " + daysFromNow + " days.";

            case 5:     //tickets
                quality = qualityTickets(initialQuality, daysFromNow, expirationDate);
                System.out.println();
                return "The " + itemName + " will have a quality rating of " + quality + " in " + daysFromNow + " days.";
        }
        return null;
    }

    public static int qualityDecrease(int initialQuality, int daysFromNow, int expirationDate) {
        int totalQuality = 0;
        int daysLeft = expirationDate - daysFromNow;

        if (daysLeft < 0) {
            int negQuality = (daysLeft * -1) * 2;
            totalQuality = initialQuality - (negQuality + expirationDate);
        } else {
            totalQuality = initialQuality - daysFromNow;
        }

        if (totalQuality < 0) {
            return 0;
        } else {
            return totalQuality;
        }
    }

    public static int qualityIncreaseDecrease(int initialQuality, int daysFromNow, int expirationDate) {
        int totalQuality = 0;
        int daysLeft = expirationDate - daysFromNow;

        if (daysLeft < 0) {
            int negQuality = (daysLeft * -1) * 2;
            totalQuality = initialQuality - (negQuality + expirationDate);
        } else {
            totalQuality = initialQuality + daysFromNow;
        }

        if (totalQuality < 0) {
            return 0;
        }
        return totalQuality;
    }

    public static int qualityTickets(int initialQuality, int daysFromNow, int expirationDate) {

        int totalQuality = 0;
        int daysLeft = expirationDate - daysFromNow;
        int tenSpot = daysFromNow - 10;

        if (daysLeft < 0) {
            return 0;
        } else if(daysFromNow > 10) {
            return initialQuality + tenSpot + 20;                 //10 * 2 for remaining 10 days
        }
        return initialQuality + daysFromNow;
    }


    public static HashMap inventoryStack() {
        LinkedHashMap<Integer, List<Object>> values = new LinkedHashMap();
        List<Object> vest = new ArrayList();
        vest.add("+5 Dexterity Vest");
        vest.add(10);
        vest.add(20);
        List<Object> brie = new ArrayList();
        brie.add("Aged Brie");
        brie.add(2);
        brie.add(0);
        List<Object> elixir = new ArrayList();
        elixir.add("Elixir of the Mongoose");
        elixir.add(5);
        elixir.add(7);
        List<Object> sulfuras = new ArrayList();
        sulfuras.add("Sulfuras");
        sulfuras.add(0);
        sulfuras.add(80);
        List<Object> passes = new ArrayList();
        passes.add("Concert backstage passes");
        passes.add(15);
        passes.add(20);
        values.put(1, vest);
        values.put(2, brie);
        values.put(3, elixir);
        values.put(4, sulfuras);
        values.put(5, passes);
        return values;
    }
}



